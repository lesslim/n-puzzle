from typing import List, Tuple


def main_of_algorithms(
    board: List[List[int]], board_size: int, heuristic: str
) -> Tuple[int, int, int, List[List[List[int]]]]:
    """
    Пока заглушка

    Main point of the algorithms.

    return:
    Total number of states ever selected in the "opened" set (complexity in time).
    Maximum number of states ever represented in memory at the same time during the search (complexity in size).
    Number of moves required to transition from the initial state to the final state, according to the search.
    The ordered sequence of states that make up the solution, according to the search.
    """
    return 0, 0, 0, [[[0]]]
