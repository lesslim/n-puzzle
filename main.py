import argparse
import time
from algorithm import main_of_algorithms
from typing import List
from parse import parse_file


def print_results(
    moves_time: int,
    moves_size: int,
    moves_num: int,
    states: List[List[List[int]]],
    print_states: bool,
):
    """
    Print results as subject said.
    """
    print("complexity in time:", moves_time)
    print("complexity in size:", moves_size)
    print("number of moves to the final state:", moves_num)
    if print_states:
        print(
            "ordered sequence of states: ", states
        )  #  тут нормально распечатаю, когда понятно будет как промежуточные ходы будут выглядеть


def is_solvable(board: List[List[int]]) -> bool:
    """
    (пока не сделано)
    Check if the puzzle can be solved.
    """
    if board:
        return True
    return False


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("puzzle", help="file with n, puzzle")
    parser.add_argument(
        "--visualization",
        "-v",
        default=False,
        action="store_true",
        help="to see the movements (doesn't implement yet)",
    )
    parser.add_argument(
        "--sequence_of_states",
        "-s",
        default=False,
        action="store_true",
        help="to see the ordered sequence of states that make up the solution",
    )
    parser.add_argument(
        "--Heuristic",
        "-H",
        type=int,
        help="heuristic functions: 1 for  Manhattan-distance, 2 for hamming_distance, 3 for ???",
        default=1,
    )
    args = parser.parse_args()

    if args.Heuristic not in [1, 2, 3]:
        print("Please select the correct heuristic.")
        exit(1)

    board_size, board = parse_file(args.puzzle)

    if not is_solvable(board):
        print("This n-puzzle is unsolvable.")
    else:
        moves_time, moves_size, moves_num, states = main_of_algorithms(
            board, board_size, args.Heuristic
        )
        print_results(
            moves_time, moves_size, moves_num, states, args.sequence_of_states
        )
        if args.visualization:
            pass
