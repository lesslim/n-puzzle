from typing import List, Tuple


def line_validation(arr: List[str]) -> List[int]:
    """
    Validation of line from file
    """
    try:
        int_arr = list(map(int, arr))
    except ValueError:
        print("The board should only contain numbers.")
        exit(1)
    return int_arr


def make_array(line: str) -> List[int]:
    """
    Make array of ints from string.
    """
    hash_ind = line.find("#")
    if hash_ind != -1:
        arr = line[:hash_ind].split()
    else:
        arr = line.split()
    int_arr = line_validation(arr)
    return int_arr


def board_validation(board_size: int, board: List[List[int]]):
    """
    Validation of board:
    checking that it contain all numbers from 0 to board_size - 1
    """
    s = set()
    for i in board:
        s.update(i)
    if len(s) != board_size or min(s) != 0 or max(s) != board_size - 1:
        print(
            f"The board is invalid (must contain all numbers from 0 to {board_size - 1})."
        )
        exit(1)


def parse_file(file: str) -> Tuple[int, List[List[int]]]:
    """
    Extract board and size of board from file.
    """
    try:
        with open(file) as f:
            lines = f.read().splitlines()
    except Exception as e:
        print("Something wrong with the file:", e.args)
        exit(1)

    size = -1
    board = []
    for i, line in enumerate(lines):
        arr = make_array(line)
        arr_len = len(arr)
        if arr_len == 0:
            continue
        if arr_len == 1 and size == -1:
            size = arr[0]
        else:
            if arr_len == size:
                board.append(arr)
            else:
                print("Board and size of board don't match at line", i + 1)
                exit(1)

    board_validation(size ** 2, board)
    return size, board
